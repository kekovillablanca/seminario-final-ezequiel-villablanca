package ModeloException;

public class ExisteException extends Exception{

	public ExisteException(String message) {
        super(message);
	}
}
