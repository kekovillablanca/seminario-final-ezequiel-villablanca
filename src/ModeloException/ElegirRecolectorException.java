package ModeloException;
@SuppressWarnings("serial")
public class ElegirRecolectorException extends Exception{
	
	public ElegirRecolectorException(String message) {
        super(message);
    }

}