package ModeloException;
@SuppressWarnings("serial")
public class FiltradoException extends Exception{
	
	public FiltradoException(String message) {
        super(message);
    }

}