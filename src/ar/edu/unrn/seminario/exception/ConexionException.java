package ar.edu.unrn.seminario.exception;

public class ConexionException  extends Exception{
	public ConexionException(String message) {
		super(message);
	}
}
