package ar.edu.unrn.seminario.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ModeloException.ApiException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.api.PersistenceApi;
import ar.edu.unrn.seminario.dto.PropietarioDTO;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.dto.UsuarioDTO;
import ar.edu.unrn.seminario.exception.StateException;

import javax.swing.JRadioButton;

public class LoginUsuario extends JFrame {

	private JPanel contentPane;
	private JFrame login;
	private JTextField ingresoContrasenia;
	private JTextField ingresoUsuario;
	private ResourceBundle labels;
	private boolean existe;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					IApi api = new PersistenceApi();
					LoginUsuario frame = new LoginUsuario(api);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginUsuario(IApi api) {
		this.login=this;
		setTitle("Ingresar cuenta");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 663, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		labels = ResourceBundle.getBundle("labels", new Locale("es"));
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		ingresoContrasenia = new JTextField();
		ingresoContrasenia.setBounds(172, 145, 114, 19);
		panel.add(ingresoContrasenia);
		ingresoContrasenia.setColumns(10);

		JLabel lblNewLabel = new JLabel(labels.getString("iniciar.sesion.label.password"));
		lblNewLabel.setBounds(74, 147, 70, 15);
		panel.add(lblNewLabel);

		JLabel lblUsername = new JLabel(labels.getString("iniciar.sesion.label.usuario"));
		lblUsername.setBounds(74, 81, 87, 15);
		panel.add(lblUsername);

		ingresoUsuario = new JTextField();
		ingresoUsuario.setBounds(172, 79, 114, 19);
		panel.add(ingresoUsuario);
		ingresoUsuario.setColumns(10);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 46, 440, 2);
		panel.add(separator);
		
		ingresoContrasenia.addKeyListener(new KeyAdapter() {
	        @Override
	        public void keyPressed(KeyEvent e) {
	        	
	            if(e.getKeyCode() == KeyEvent.VK_ENTER){
	            	try {
						 existe = api.ingresarUsuario(ingresoUsuario.getText(), ingresoContrasenia.getText());
						if (existe == true) {
								
								String nombreRol=api.usuarioTieneRol(ingresoUsuario.getText());
								UsuarioDTO usuario=api.obtenerUsuario(ingresoUsuario.getText());
								VentanaPrincipal ventanaPrincipal = new VentanaPrincipal(usuario,nombreRol,api,labels);
								ventanaPrincipal.setVisible(true);
								login.setVisible(false);
								JOptionPane.showMessageDialog(null, labels.getString("alta.usuario.mensaje.informativo"),
								"Info", JOptionPane.INFORMATION_MESSAGE);
							
							
							
						}
					}

					catch (ApiException e1) {
						JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, e2.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						
					}
	            }
	        }

	    });
		Button button = new Button(labels.getString("iniciar.sesion.titulo"));
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Khmer OS System", Font.PLAIN, 20));
		button.setBackground(Color.BLACK);
		button.addActionListener((a)-> {

				try {
					existe = api.ingresarUsuario(ingresoUsuario.getText(), ingresoContrasenia.getText());
					if (existe == true) {
							
							String nombreRol=api.usuarioTieneRol(ingresoUsuario.getText());
							UsuarioDTO usuario=api.obtenerUsuario(ingresoUsuario.getText());
							VentanaPrincipal ventanaPrincipal = new VentanaPrincipal(usuario,nombreRol,api,labels);
							ventanaPrincipal.setVisible(true);
							login.setVisible(false);
							JOptionPane.showMessageDialog(null, labels.getString("alta.usuario.mensaje.informativo"),
							"Info", JOptionPane.INFORMATION_MESSAGE);
						
						
						
					}
				}

				catch (ApiException | StateException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		);
		button.setBounds(160, 200, 158, 23);
		panel.add(button);

		JLabel lblRegistrarcuenta = new JLabel(labels.getString("iniciar.sesion.label.registrarse"));
		lblRegistrarcuenta.setForeground(Color.BLUE);
		lblRegistrarcuenta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				AltaUsuario altaUsuario = new AltaUsuario(api,true, labels);
				altaUsuario.setVisible(true);

			}
		});
		lblRegistrarcuenta.setBounds(182, 175, 152, 19);
		panel.add(lblRegistrarcuenta);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton(labels.getString("ventana.principal.menu.item.espaniol"));
		rdbtnNewRadioButton.addActionListener((e)-> {

				labels = ResourceBundle.getBundle("labels", new Locale("es"));
			
			}
		);
		rdbtnNewRadioButton.setBounds(414, 175, 96, 23);
		panel.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton(labels.getString("ventana.principal.menu.item.ingles")); //$NON-NLS-1$
		rdbtnNewRadioButton_1.addActionListener((e)-> {
			
		
				labels = ResourceBundle.getBundle("labels", new Locale("en"));
			this.update(getGraphics());
			
		});
		rdbtnNewRadioButton_1.setBounds(414, 214, 149, 23);
		panel.add(rdbtnNewRadioButton_1);
		
		ButtonGroup group = new ButtonGroup();
	    group.add(rdbtnNewRadioButton);
	    group.add(rdbtnNewRadioButton_1);
	    
	    
	}
	
}
