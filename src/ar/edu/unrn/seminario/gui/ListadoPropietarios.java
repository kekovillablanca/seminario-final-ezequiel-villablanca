package ar.edu.unrn.seminario.gui;

import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import ModeloException.ApiException;
import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.OrdenDePedidoDTO;
import ar.edu.unrn.seminario.dto.PropietarioDTO;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.dto.UsuarioDTO;
import ar.edu.unrn.seminario.dto.ViviendaDTO;
import ar.edu.unrn.seminario.herramienta.ModificacionesReload;

public class ListadoPropietarios extends JFrame {

    private JPanel contentPane;
    private JTable table;
    DefaultTableModel modelo;
    IApi api;
    private JButton btnNewButton;
    private Panel panel;
    private JTextField textField;
    private JTextField textField_1;
    private boolean encontro = false;

    /**
     * Create the frame.
     *
     * @throws NullException
     * @throws SQLException
     */
    public ListadoPropietarios(UsuarioDTO usuario,String nombreRol,IApi api,ResourceBundle labels){
    	
        this.api = api;

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 800, 350);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
    
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(12, 5, 487, 231);
        contentPane.add(scrollPane);

        this.setTitle(labels.getString("listado.propietarios.ventana"));
        
        
        table = new JTable();
        String[] titulos = { labels.getString("listado.propietarios.titulo.nombre"),
                labels.getString("listado.propietarios.titulo.apellido"),
                labels.getString("listado.propietarios.titulo.dni")};

        modelo = new DefaultTableModel(new Object[][] {}, titulos);

        // Obtiene la lista de usuarios a mostrar
        if(RolDTO.administrador().equals(nombreRol)) {
        List<PropietarioDTO> propietarios;
		try {
			propietarios = api.obtenerPropietarios();
			for (PropietarioDTO p : propietarios) {

            modelo.addRow(new Object[] { p.getNombre(), p.getApellido(), p.getDni() });
        }
		} catch (ApiException e1) {
			JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
        }
        if(RolDTO.usuario().equals(nombreRol)) {
        	List<PropietarioDTO> propietarios;
    		try {
    			propietarios = api.obtenerPropietarios(usuario);
    			for (PropietarioDTO p : propietarios) {

                modelo.addRow(new Object[] { p.getNombre(), p.getApellido(), p.getDni() });
            }
    		} catch (ApiException e1) {
    			JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    		}
        }
        // Agrega los usuarios en el model
        

        table.setModel(modelo);
        scrollPane.setViewportView(table);

        panel = new Panel();
        panel.setBounds(5, 236, 790, 35);
        contentPane.add(panel);

        btnNewButton = new JButton(labels.getString("listado.propietarios.button.cerrar"));
        btnNewButton.addActionListener((e)-> {
           
                dispose();
            }
        );
        panel.add(btnNewButton);

        Panel panel_1 = new Panel();
        panel_1.setBounds(527, 5, 263, 231);
        contentPane.add(panel_1);
        panel_1.setLayout(null);



        JButton btnNewButton_1 = new JButton(labels.getString("listado.propietarios.button.modificar"));
        btnNewButton_1.addActionListener((a)-> {

                try {

                    int tablaElegida = table.getSelectedRow();
                    if (tablaElegida >= 0) {
                        String numeroDNI = (String) table.getValueAt(tablaElegida, 2);

                        
                        PropietarioDTO propietarioDTO = api.obtenerPropietario(numeroDNI);
                        
                        

                        ModificarDatosPropietario modificarDatosPropietario= new ModificarDatosPropietario(api, propietarioDTO,labels,this);
                        modificarDatosPropietario.setVisible(true);
                       
                        
                    } else {
                        throw new Exception("Ha ocurrido un error no se ha elegido ningun propietario");
                    }
                } catch (Exception e) {
                	e.printStackTrace();
                    JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }

            }
        );
        btnNewButton_1.setBounds(32, 159, 188, 25);
        panel_1.add(btnNewButton_1);
        
        /*JButton btnNewButton_2 = new JButton(labels.getString("listado.orden.boton.actualizar"));
        
        btnNewButton_2.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		reloadGrid();
        	}
        });
        btnNewButton_2.setBounds(32, 200, 188, 21);
        panel_1.add(btnNewButton_2);
        System.out.println(ModificacionesReload.getReload());
        */

    }
    public void reloadGrid() {

		table.getModel();

		// Obtiene la lista de usuarios a mostrar List<PedidoDTO> pedidos =
		List<PropietarioDTO> listPropietario;
		try {
			listPropietario = api.obtenerPropietarios();
			modelo.setRowCount(0);

		for (PropietarioDTO p : listPropietario) {
			modelo.addRow(new Object[] { p.getNombre(), p.getApellido(), p.getDni() });

		}
		} catch (ApiException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}

		

	}
}