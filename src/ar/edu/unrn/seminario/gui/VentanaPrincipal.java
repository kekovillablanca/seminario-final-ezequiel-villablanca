package ar.edu.unrn.seminario.gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.api.PersistenceApi;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.dto.UsuarioDTO;

import javax.swing.JToolBar;
import javax.swing.JDesktopPane;

public class VentanaPrincipal extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IApi api = new PersistenceApi();
					VentanaPrincipal frame = new VentanaPrincipal(null,null,api,null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal(UsuarioDTO usuario,String nombreRol,IApi api,ResourceBundle labels) {
		// ii8n
		// ResourceBundle labels=ResourceBundle.getBundle("labels");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 863, 300);
		
		// ResourceBundle labels = ResourceBundle.getBundle("labels");
		JMenuBar menuBar = new JMenuBar();
		menuBar.setToolTipText("texto");
		setJMenuBar(menuBar);
		/*
		 * JMenuItem ListadoNewMenuItem = new
		 * JMenuItem(labels.getString("ventana.principal.menu.item.listado"));
		 * ListadoNewMenuItem.addActionListener(e->{ new
		 * ListarUsuario(api).setVisible(true); new
		 * ListarUsuario(api).setVisible(false);
		 * 
		 * 
		 * });
		 */
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	
							if(RolDTO.administrador().equals(nombreRol)) {
						
							

						JMenu campaniasMenu = new JMenu(labels.getString("ventana.principal.menu.item.campania")); //$NON-NLS-1$
						menuBar.add(campaniasMenu);
						
								JMenuItem creaBeneficioMenuItem = new JMenuItem(
										labels.getString("ventana.principal.menu.item.campania.registrar.beneficio")); //$NON-NLS-1$
								creaBeneficioMenuItem.addActionListener((e)-> {
									
										CrearBeneficio crear = new CrearBeneficio(api, labels);
										crear.setVisible(true);
									
								});
								campaniasMenu.add(creaBeneficioMenuItem);
								
										JMenuItem campaniaMenuItem = new JMenuItem(
												labels.getString("ventana.principal.menu.item.campania.registrar.campania")); //$NON-NLS-1$
										campaniaMenuItem.addActionListener((e)-> {
										
												RegistrarCampania registrar = new RegistrarCampania(api, labels);
												registrar.setVisible(true);
											
										});
										campaniasMenu.add(campaniaMenuItem);
										
												
		
				JMenu UsuariosNewMenu = new JMenu(labels.getString("ventana.principal.menu.usuarios"));
				menuBar.add(UsuariosNewMenu);
				
						JMenuItem AltaModificacionNewMenuItem = new JMenuItem(
								labels.getString("ventana.principal.menu.alta.modificacion"));
						AltaModificacionNewMenuItem.addActionListener(e -> new AltaUsuario(api,false,labels).setVisible(true));
						
								UsuariosNewMenu.add(AltaModificacionNewMenuItem);
								
										JMenuItem ListadoNewMenuItem = new JMenuItem(labels.getString("ventana.principal.menu.item.listado"));
										ListadoNewMenuItem.addActionListener(e -> new ListarUsuario(api, labels).setVisible(true));
										UsuariosNewMenu.add(ListadoNewMenuItem);

		JMenu RolesNewMenu = new JMenu(labels.getString("ventana.principal.menu.rol"));
		menuBar.add(RolesNewMenu);

		JMenuItem AltaRolNewMenuItem = new JMenuItem(labels.getString("ventana.principal.menu.item.rol"));
		AltaRolNewMenuItem.addActionListener(e -> new AltaRol(api,labels).setVisible(true)
				);

		RolesNewMenu.add(AltaRolNewMenuItem);

		/*JMenuItem ListarNewMenuItem = new JMenuItem(labels.getString("ventana.principal.menu.lista.rol"));
		ListarNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		RolesNewMenu.add(ListarNewMenuItem);*/

		/*JMenu ViviendasNewMenu = new JMenu(labels.getString("ventana.principal.menu.viviendas"));
		menuBar.add(ViviendasNewMenu);

		JMenuItem RegistrarViviendaNewMenuItem = new JMenuItem(
				labels.getString("ventana.principal.menu.item.registrar.vivienda"));
		RegistrarViviendaNewMenuItem.addActionListener((e)-> {
			
				RegistrarVivienda registrarVivienda = new RegistrarVivienda(nombreRol,usuario,api, labels);
				registrarVivienda.setVisible(true);
			}
	);
		ViviendasNewMenu.add(RegistrarViviendaNewMenuItem);

		JMenuItem ListarViviendaNewMenuItem = new JMenuItem(
				labels.getString("ventana.principal.menu.item.listar.vivienda"));
		ListarViviendaNewMenuItem.addActionListener(e -> {

			ListadoVivienda listado;
		
				listado = new ListadoVivienda(usuario,nombreRol,api, labels);
				listado.setVisible(true);
			
			

		});
		ViviendasNewMenu.add(ListarViviendaNewMenuItem);

		JMenuItem ListadoPropietariosMenuItem = new JMenuItem(
				labels.getString("ventana.principal.menu.item.listado.propietarios"));
		ListadoPropietariosMenuItem.addActionListener((e)-> {

				
					ListadoPropietarios listadoPropietarios = new ListadoPropietarios(usuario,nombreRol,api, labels);

					listadoPropietarios.setVisible(true);
				
			
		});
		ViviendasNewMenu.add(ListadoPropietariosMenuItem);
*/
		JMenu mnNewMenu = new JMenu(labels.getString("ventana.principal.menu.item.pedidos"));
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem(labels.getString("ventana.principal.menu.item.pedido.de.retiro")); //$NON-NLS-1$
		mntmNewMenuItem.addActionListener((e)->{
			
				ListadoDePedidos listadoPedido;
				
					listadoPedido = new ListadoDePedidos(api,labels);
					listadoPedido.setVisible(true);
				
			
		});
		mnNewMenu.add(mntmNewMenuItem);

		JMenu registrarRecolector = new JMenu(labels.getString("ventana.principal.menu.registrar.recolector")); //$NON-NLS-1$
		menuBar.add(registrarRecolector);

		JMenuItem registrar = new JMenuItem(labels.getString("ventana.principal.menu.item.registrar.recolector")); //$NON-NLS-1$
		registrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				RegistrarRecolector registrar = new RegistrarRecolector(api,labels);
				registrar.setVisible(true);

			}
		});
		registrarRecolector.add(registrar);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem(labels.getString("ventana.principal.menu.item.listado.recolectores")); //$NON-NLS-1$
		mntmNewMenuItem_2.addActionListener((e)-> {
			
			
					ListarRecolectores listarRecolectores;
					
						listarRecolectores = new ListarRecolectores(api,labels);
						listarRecolectores.setVisible(true);
				
					
				 
				
			
		});
		registrarRecolector.add(mntmNewMenuItem_2);
		
							}

		JButton SalirNewButton = new JButton(labels.getString("ventana.principal.menu.item.salir"));
		SalirNewButton.addActionListener((e)-> {
			
				dispose(); 
			
		});
		SalirNewButton.setBounds(179, 217, 89, 23);
		contentPane.add(SalirNewButton);
		if(RolDTO.recolector().equals(nombreRol)) {
			
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 863, 300);
			
		

			JMenu mnNewMenu_1 = new JMenu(labels.getString("ventana.principal.menu.item.orden")); //$NON-NLS-1$
			menuBar.add(mnNewMenu_1);
			
					JMenuItem mntmNewMenuItem_1 = new JMenuItem(labels.getString("ventana.principal.menu.listado.ordenes"));
					mnNewMenu_1.add(mntmNewMenuItem_1);
					
		
					mntmNewMenuItem_1.addActionListener((e)-> {
						
							ListadoOrden listado = new ListadoOrden(usuario,api, labels);
							listado.setVisible(true);
						}
					);
					
			}
		
			
			if(RolDTO.usuario().equals(nombreRol)) {
				JMenuItem canjearMenuItem = new JMenuItem(labels.getString("ventana.principal.menu.item.campania.canjear.beneficio"));
				canjearMenuItem.addActionListener((e)->{
					
					ListadoDePropietariosParaCanjeo listado;										
					listado = new ListadoDePropietariosParaCanjeo(usuario,api,labels);
					listado.setVisible(true);						});
					
						



					JMenu ViviendasNewMenu = new JMenu(labels.getString("ventana.principal.menu.viviendas"));
					menuBar.add(ViviendasNewMenu);

					JMenuItem RegistrarViviendaNewMenuItem = new JMenuItem(
							labels.getString("ventana.principal.menu.item.registrar.vivienda"));
					RegistrarViviendaNewMenuItem.addActionListener((e)->{
				
							RegistrarVivienda registrarVivienda = new RegistrarVivienda(nombreRol,usuario,api,labels);
							registrarVivienda.setVisible(true);
					}
					);
					ViviendasNewMenu.add(RegistrarViviendaNewMenuItem);

					JMenuItem ListarViviendaNewMenuItem = new JMenuItem(
							labels.getString("ventana.principal.menu.item.listar.vivienda"));
					ListarViviendaNewMenuItem.addActionListener(e -> {

						ListadoVivienda listado;
					
							listado = new ListadoVivienda(usuario,nombreRol,api,labels);
							listado.setVisible(true);
						
						

					});
					ViviendasNewMenu.add(ListarViviendaNewMenuItem);
					
					JMenu mnCanjear = new JMenu(labels.getString("ventana.principal.menu.item.campania.canjear.beneficio")); //$NON-NLS-1$
					menuBar.add(mnCanjear);
					
					JMenuItem mntmCanjearPuntos = new JMenuItem(labels.getString("ventana.principal.menu.item.campania.canjear.beneficio")); //$NON-NLS-1$
					mntmCanjearPuntos.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							ListadoDePropietariosParaCanjeo listado;
							
							listado = new ListadoDePropietariosParaCanjeo(usuario,api,labels);
							listado.setVisible(true);
						}
					});
					mnCanjear.add(mntmCanjearPuntos);

				
					JMenuItem ListadoPropietariosMenuItem = new JMenuItem(
							labels.getString("ventana.principal.menu.item.listado.propietarios"));
					ListadoPropietariosMenuItem.addActionListener((e)-> {

							
								ListadoPropietarios listadoPropietarios = new ListadoPropietarios(usuario,nombreRol,api, labels);

								listadoPropietarios.setVisible(true);
							
						
					});
					ViviendasNewMenu.add(ListadoPropietariosMenuItem);
				
			}
		
							

	}
}
