package ar.edu.unrn.seminario.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.api.PersistenceApi;
import ar.edu.unrn.seminario.dto.RolDTO;
import ModeloException.ApiException;
import ModeloException.DataEmptyException;
import ModeloException.JdbcException;
import ModeloException.ExisteException;
import ModeloException.NullException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.ResourceBundle;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class RegistrarRecolector extends JFrame {

	private JPanel contentPane;
	private JTextField nombreField;
	private JTextField apellidoField;
	private JTextField dniField;
	private JTextField legajoField;
	private JTextField usuarioTextField;
	private JTextField contrasenaTextField;
	
	private JTextField emailTextField;
	private IApi api;

	/**
	 * Launch the application.
	 */

	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IApi api = new PersistenceApi();
					RegistrarRecolector frame = new RegistrarRecolector(api);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistrarRecolector(IApi api,ResourceBundle labels) {
		this.api = api;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 632, 313);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		this.setTitle(labels.getString("registrar.recolector.ventana"));

		JLabel tituloLabel = new JLabel(labels.getString("ventana.principal.menu.registrar.nuevo.recolector"));
		tituloLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tituloLabel.setBounds(10, 10, 183, 14);
		contentPane.add(tituloLabel);

		JLabel nombreLabel = new JLabel(labels.getString("registro.recolector.label.nombre"));
		nombreLabel.setBounds(20, 34, 60, 14);
		contentPane.add(nombreLabel);

		JLabel apellidoLabel = new JLabel(labels.getString("registro.recolector.label.apellido"));
		apellidoLabel.setBounds(148, 34, 50, 14);
		contentPane.add(apellidoLabel);

		JLabel emailLabel = new JLabel(labels.getString("alta.usuario.label.email"));
		emailLabel.setBounds(355, 172, 56, 16);
		contentPane.add(emailLabel);

		emailTextField = new JTextField();
		emailTextField.setBounds(355, 198, 160, 22);
		contentPane.add(emailTextField);
		emailTextField.setColumns(10);
		usuarioTextField = new JTextField();
		usuarioTextField.setBounds(355, 83, 160, 22);
		contentPane.add(usuarioTextField);
		usuarioTextField.setColumns(10);

		contrasenaTextField = new JTextField();
		contrasenaTextField.setBounds(355, 140, 160, 22);
		contentPane.add(contrasenaTextField);
		contrasenaTextField.setColumns(10);
		
		JLabel usuarioLabel = new JLabel(labels.getString("alta.usuario.label.usuario"));
		
		 this.setTitle(labels.getString("alta.usuario.ventana"));
		
		usuarioLabel.setBounds(352, 57, 76, 16);
		contentPane.add(usuarioLabel);

		JLabel contrasenaLabel = new JLabel(labels.getString("alta.usuario.label.password"));
		contrasenaLabel.setBounds(355, 115, 93, 16);
		contentPane.add(contrasenaLabel);

		JButton registrarButton = new JButton(labels.getString("registro.recolector.button.registrar"));
		registrarButton.addActionListener((e)-> {
		

				try {
					
						
							api.registrarRecolector(nombreField.getText(), apellidoField.getText(),
									dniField.getText(), legajoField.getText(), emailTextField.getText(),usuarioTextField.getText(),contrasenaTextField.getText(),RolDTO.recolector());
						
					JOptionPane.showMessageDialog(null, "Recolector registrado con exito!", "Info",
							JOptionPane.INFORMATION_MESSAGE);
					setVisible(false);
					// dispose();
				} catch (ApiException | ExisteException | DataEmptyException  e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}

			}
		);

		registrarButton.setBounds(339, 253, 89, 23);
		contentPane.add(registrarButton);

		JButton cancelarButton = new JButton(labels.getString("registro.recolector.button.cancelar"));
		cancelarButton.addActionListener((e)-> {
	
				setVisible(false);
			
		});
		cancelarButton.setBounds(148, 253, 89, 23);
		contentPane.add(cancelarButton);

		nombreField = new JTextField();
		nombreField.setBounds(10, 58, 86, 20);
		contentPane.add(nombreField);
		nombreField.setColumns(10);

		apellidoField = new JTextField();
		apellidoField.setBounds(148, 58, 86, 20);
		contentPane.add(apellidoField);
		apellidoField.setColumns(10);

		JLabel dniLabel = new JLabel(labels.getString("registro.recolector.label.dni"));
		dniLabel.setBounds(20, 87, 46, 14);
		contentPane.add(dniLabel);

		dniField = new JTextField();
		dniField.setBounds(10, 111, 86, 20);
		contentPane.add(dniField);
		dniField.setColumns(10);

		JLabel legajoLabel = new JLabel(labels.getString("registro.recolector.label.legajo"));
		legajoLabel.setBounds(158, 88, 46, 14);
		contentPane.add(legajoLabel);

		legajoField = new JTextField();
		legajoField.setBounds(148, 111, 86, 20);
		contentPane.add(legajoField);
		legajoField.setColumns(10);
	}
}
