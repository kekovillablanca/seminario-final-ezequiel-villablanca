package ar.edu.unrn.seminario.modelo;

import java.util.ArrayList;

import ModeloException.NullException;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

public class Recolector {
	private String nombre;
	private String apellido;
	
	private String dni;
	private String turno;
	private String legajo;
	private Usuario usuario;
	private ArrayList<Visita> visitasDelRecolector = new ArrayList<Visita>();

	public Recolector(String nombre, String apellido, String dni, String legajo) throws NullException{

		if (VerificacionDatos.stringIsEmpty(nombre))
			throw new NullException("nombre");
		if(VerificacionDatos.stringIsEmpty(apellido))
			throw new NullException("apellido");
		if(VerificacionDatos.stringIsEmpty(dni))
			throw new NullException("dni");
		if(VerificacionDatos.stringIsEmpty(legajo))
			throw new NullException("legajo");
		//if(esDatoNulo(turno))
			//throw new NullException("turno");
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.legajo = legajo;
		//this.turno = turno;
	}

	public void agregarVisita(Visita nueva) {
		this.visitasDelRecolector.add(nueva);
	}
	public void setUsuario(Usuario usuario) {
		this.usuario=usuario;
		
	}
	public Usuario getUsuario() {
		return usuario;
	}

	public String obtenerLegajo() {
		return this.legajo;
	}

	public void cambiarNombre(String nombre) {
		this.nombre = nombre;
	}

	public String obtenerNombre() {
		return this.nombre;
	}

	public void cambiarApellido(String apellido) {
		this.apellido = apellido;
	}

	public String obtenerApellido() {

		return this.apellido;
	}


	public void cambiarTurno(String turno) {
		this.turno = turno;
	}

	public String obtenerTurno() {
		return this.turno;
	}
	public String obtenerDni() {
		return this.dni;
	}
	
	
	// Equals-> legajo

}

