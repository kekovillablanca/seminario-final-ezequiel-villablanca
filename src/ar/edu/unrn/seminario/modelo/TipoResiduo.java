package ar.edu.unrn.seminario.modelo;

import ModeloException.NullException;

public class TipoResiduo {

	private String nombre;

	private int idTipo;
	private int puntosResiduos;

	public TipoResiduo(String nombre) throws NullException {

		

		this.nombre = nombre;

	}

	public TipoResiduo(int idTipo, String nombre, int puntosResiduos) throws NullException {

		this.puntosResiduos = puntosResiduos;
		this.idTipo = idTipo;

		this.nombre = nombre;

	}

	

	public int getId() {
		return this.idTipo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public int getPuntosResiduos() {
		return puntosResiduos;
	}

	public void setPuntosResiduos(int puntosResiduos) {
		this.puntosResiduos = puntosResiduos;
	}

}
