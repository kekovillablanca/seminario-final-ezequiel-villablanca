package ar.edu.unrn.seminario.modelo;

import java.util.Objects;

import ModeloException.DataEmptyException;
import ModeloException.EmailException;
import ModeloException.EstadoException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

public class Usuario {
	private int id;
    private String username;
    private String email;
    private String password;
    private Rol rol;
    private boolean estado;


    public Usuario(String username, String password, String email,
                   Rol rol) throws DataEmptyException, EmailException{
        
    	this.validarDatos(username,password,email,rol);
        this.username= username;
        this.password=password;
        this.email= email;
        this.rol= rol;
       


    }
   
    public Usuario(String username, String password, String email,
            Rol rol,boolean estado) throws DataEmptyException, EmailException{
    	
    	this.validarDatos(username,password,email,rol);
    	this.username= username;
    	this.password=password;
    	this.email= email;
    	this.rol= rol;
    	
    	this.estado=estado;


}
    public Usuario(int id,String username, String password, String email,
            Rol rol,boolean estado) throws DataEmptyException, EmailException{
    	
    	this.validarDatos(username,password,email,rol);
    	this.username= username;
    	this.password=password;
    	this.email= email;
    	this.rol= rol;
    	this.id=id;
    	this.estado=estado;


}
    private void validarDatos(String username, String password, String email,
            Rol rol) throws DataEmptyException, EmailException
    {
    	if(VerificacionDatos.stringIsEmpty(username)) {
    		throw new DataEmptyException("usuario es null");
    	}
    	if(VerificacionDatos.stringIsEmpty(password)) {
    		throw new DataEmptyException("password es null");
    	}
    	if(VerificacionDatos.stringIsEmpty(email)) {
    		throw new DataEmptyException("email es null");
    	}
    	if(VerificacionDatos.IsEmpty(rol)) {
    		throw new DataEmptyException("rol es null");
    	}
    	if(VerificacionDatos.IsIncorrectEmail(email)) {
    		throw new EmailException("el email no es correcto");
    	}
    }

    public String getNombreUsuario() {
        return username;
    }

    public String getContrasenia() {
        return password;
    }

    public String getEmail() {
        return email;
    }
    public void setId(int id) {
    	this.id=id;
    }
    public int getId() {
    	return this.id;
    }
    public Rol getRol() {
        return this.rol;
    }

    public void cambiarEstado(boolean estado)throws EstadoException {
    	if(estado==this.estado) {
    		throw new EstadoException("el estado se encuentra igual no se puede cambiar");
    	}
    	this.estado=estado;
    }
    public boolean estaActivo() {
        return this.estado;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nombreUsuario='" + username + '\'' +
                ", email='" + email + '\'' +
                ", contrasenia='" + password + '\'' +
                ", rol=" + rol +
                ", estado=" + estado +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return estado == usuario.estado && Objects.equals(username, usuario.username) && Objects.equals(email, usuario.email) && Objects.equals(password, usuario.password) && Objects.equals(rol, usuario.rol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, email, password, rol, estado);
    }
}
