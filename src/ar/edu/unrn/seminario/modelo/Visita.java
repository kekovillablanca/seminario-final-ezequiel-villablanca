package ar.edu.unrn.seminario.modelo;

import java.util.Date;
import java.util.List;

import ModeloException.NullException;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

public class Visita {
	private Date diaConcurrido;
	private String observacion;
	private List<Residuo> residuos;

	public Visita(Date diaConcurrido, String observacion, List<Residuo> residuos) throws NullException{
		if(VerificacionDatos.IsDateNull(diaConcurrido))
			throw new NullException("dia es vacio");
		if(VerificacionDatos.stringIsNull(observacion))
			throw new NullException("observacion es vacio");
		if(VerificacionDatos.IsEmpty(residuos))
			throw new NullException("residuos es vacio");
		this.diaConcurrido = diaConcurrido;
		this.observacion = observacion;
		this.residuos = residuos;
	}

	public void agregarObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Date getDiaConcurrido() {
		return diaConcurrido;
	}

	public void setDiaConcurrido(Date diaConcurrido) {
		this.diaConcurrido = diaConcurrido;
	}

	public String getObservacion() {
		return observacion;
	}

	public List<Residuo> getResiduos() {
		return residuos;
	}

	public void setResiduos(List<Residuo> residuos) {
		this.residuos = residuos;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
}
