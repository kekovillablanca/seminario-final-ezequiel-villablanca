package ar.edu.unrn.seminario.modelo;

import java.util.Date;

import ModeloException.DataEmptyException;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

public class Canjeo {
private Date fechaCanejo;
private Propietario propietario;
private Beneficio beneficio;

public Canjeo(Date fechaCanjeo,Propietario propietario,Beneficio beneficio) throws DataEmptyException {
	
	if(VerificacionDatos.IsDateNull(fechaCanjeo)) {
		throw new DataEmptyException("la fecha es nula");
	}
	if(VerificacionDatos.IsEmpty(propietario)) {
		throw new DataEmptyException("el propietario es null");
	}
	if(VerificacionDatos.IsEmpty(beneficio)) {
		throw new DataEmptyException("el beneficio es null");
	}
	this.fechaCanejo=fechaCanjeo;
	this.propietario=propietario;
	this.beneficio=beneficio;
}

public Date getFechaCanejo() {
	return fechaCanejo;
}

public void setFechaCanejo(Date fechaCanejo) {
	this.fechaCanejo = fechaCanejo;
}

public Propietario getPropietario() {
	return propietario;
}

public void setPropietario(Propietario propietario) {
	this.propietario = propietario;
}

public Beneficio getBeneficio() {
	return beneficio;
}

public void setBeneficio(Beneficio beneficio) {
	this.beneficio = beneficio;
}
}
