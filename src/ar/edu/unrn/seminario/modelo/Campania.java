package ar.edu.unrn.seminario.modelo;
import ar.edu.unrn.seminario.herramienta.Fecha;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

import java.util.ArrayList;
import java.util.Date;
import ModeloException.NullException;

public class Campania {
	private String nombre;
	private Date fechaInicio;
	private Date fechaFin;
	private ArrayList<Beneficio> catalogo = new ArrayList<Beneficio>();
	private int idCampania;

	public Campania(String nombre, Date fechaInicio, Date fechaFin) throws NullException {

		if (VerificacionDatos.IsEmpty(nombre))
			throw new NullException("Nombre vacio");
		if (VerificacionDatos.IsDateNull(fechaInicio))
			throw new NullException("Fecha de inicio vacia");
		if (VerificacionDatos.IsDateNull(fechaFin)) {
			throw new NullException("Fecha de finalizacion vacia");
		}
		this.fechaFin = fechaFin;
		this.nombre = nombre;
		this.fechaInicio = fechaInicio;
	}
	
	public Campania(String nombre, Date fechaInicio, Date fechaFin, int idCampania) throws NullException {

		if (VerificacionDatos.IsEmpty(nombre))
			throw new NullException("Nombre vacio");
		if (VerificacionDatos.IsDateNull(fechaInicio))
			throw new NullException("Fecha de inicio vacia");
		if (VerificacionDatos.IsDateNull(fechaFin)) {
			throw new NullException("Fecha de finalizacion vacia");
		}
		this.fechaFin = fechaFin;
		this.nombre = nombre;
		this.fechaInicio = fechaInicio;
		this.idCampania = idCampania;
	}
	
	public Campania(String nombre, Date fechaInicio, Date fechaFin, ArrayList<Beneficio> catalogo) throws NullException {

		if (VerificacionDatos.IsEmpty(nombre))
			throw new NullException("Nombre vacio");
		if (VerificacionDatos.IsDateNull(fechaInicio))
			throw new NullException("Fecha de inicio vacia");
		if (VerificacionDatos.IsDateNull(fechaFin)) {
			throw new NullException("Fecha de finalizacion vacia");
		}
		if (this.isEmptyArray(catalogo))
			throw new NullException("catalogo vacio");
		this.fechaFin = fechaFin;
		this.nombre = nombre;
		this.fechaInicio = fechaInicio;
		this.catalogo = catalogo;
	}
	
	public Campania(String nombre, Date fechaInicio, Date fechaFin, ArrayList<Beneficio> catalogo, Integer idCampania) throws NullException {

		if (VerificacionDatos.IsEmpty(nombre))
			throw new NullException("Nombre vacio");
		if (VerificacionDatos.IsDateNull(fechaInicio))
			throw new NullException("Fecha de inicio vacia");
		if (VerificacionDatos.IsDateNull(fechaFin)) {
			throw new NullException("Fecha de finalizacion vacia");
		}
		this.fechaFin = fechaFin;
		this.nombre = nombre;
		this.fechaInicio = fechaInicio;
		this.catalogo = catalogo;
		this.idCampania = idCampania;
	}


	
	public Date obtenerFechaFin() {
		return this.fechaFin;
	}
	
	public Date obtenerFechaInicio() {
		return this.fechaInicio;
	}
	
	public String obtenerNombre() {
		return this.nombre;
	}
	
	public Integer obtenerId() {
		return this.idCampania;
	}
	
	public ArrayList<Beneficio> obtenerBeneficios(){
		return this.catalogo;
	}
	
	public boolean estaVigente() {
		Date hoy=Fecha.fechaHoy();

		if(Fecha.esMayorOIgual(hoy, this.fechaInicio) & Fecha.esMenorOIgual(hoy, fechaFin))
			return true;
		else
			return false;
			
		
		
	}
	private boolean isEmptyArray(ArrayList<Beneficio> catalogo) {
		return catalogo.isEmpty() || catalogo.size()==0;
	}
	
	
}
