package ar.edu.unrn.seminario.modelo;

import java.util.Date;
import java.util.List;

import ModeloException.NullException;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

public class Pedido {

	private Vivienda vivienda; // se obtiene mediante el numero de vivienda
	private Date fecha;
	private List<Residuo> residuo;
	private boolean vehículo; // true: se requiere vehículo de carga pesa, tratar con checkbox en vista
	private String observacion;
	private int idPedidoRetiro;
	private OrdenDePedido OrdenDePedido;

	public Pedido(Vivienda vivienda, Date fecha, List<Residuo> residuo, boolean vehiculo, String observacion)
			throws NullException {

		if (VerificacionDatos.IsEmpty(vivienda))
			throw new NullException("vivienda");

		if (VerificacionDatos.IsDateNull(fecha))
			throw new NullException("fecha");

		if (VerificacionDatos.IsEmpty(residuo))
			throw new NullException("residuo");



		this.vivienda = vivienda;
		this.observacion = observacion;
		this.vehículo = vehiculo;
		this.fecha = fecha;
		this.residuo = residuo;
	}

	public Pedido(int idPedido, Vivienda vivienda) throws NullException {

		if (VerificacionDatos.IsEmpty(idPedido))
			throw new NullException("id pedido");
		this.idPedidoRetiro = idPedido;
	}

	public Pedido(int idPedidoRetiro, Vivienda vivienda, Date fecha, List<Residuo> residuo, boolean vehiculo,
			String observacion) throws NullException {
		if (VerificacionDatos.IsEmpty(idPedidoRetiro))
			throw new NullException("idpedido");
		if (VerificacionDatos.IsEmpty(vivienda))
			throw new NullException("vivienda");

		if (VerificacionDatos.IsDateNull(fecha))
			throw new NullException("fecha");

		if (VerificacionDatos.IsEmpty(residuo))
			throw new NullException("residuo");

		


		this.vehículo = vehiculo;
		this.observacion = observacion;
		this.vivienda = vivienda;
		this.residuo = residuo;
		this.fecha = fecha;
		this.idPedidoRetiro = idPedidoRetiro;
	}

	public Pedido(int idPedidoRetiro, Date fecha, List<Residuo> residuo, boolean vehiculo, String observacion)
			throws NullException {
		if (VerificacionDatos.IsEmpty(idPedidoRetiro))
			throw new NullException("idpedido");

		if (VerificacionDatos.IsDateNull(fecha))
			throw new NullException("fecha");

		if (VerificacionDatos.IsEmpty(residuo))
			throw new NullException("residuo");



		
		this.vehículo = vehiculo;
		this.observacion = observacion;

		this.residuo = residuo;
		this.fecha = fecha;
		this.idPedidoRetiro = idPedidoRetiro;
	}
	public Pedido(int nroPedido) {
		this.idPedidoRetiro = nroPedido;
	}

	public OrdenDePedido getOrden() {
		return this.OrdenDePedido;
	}
	public void setOrdenPedido(OrdenDePedido orden) {
		 this.OrdenDePedido=orden;
	}
	public Date getFecha() {
		return this.fecha;
	}

	public List<Residuo> getResiduo() {
		return this.residuo;
	}

	public String getObservacion() {
		return observacion;
	}

	public boolean getVehiculo() {
		return this.vehículo;
	}

	public Vivienda getVivienda() {
		// TODO Auto-generated method stub
		return this.vivienda;
	}

	public int getidPedidoRetiro() {
		// TODO Auto-generated method stub
		return this.idPedidoRetiro;
	}

	public void setVivienda(Vivienda vivienda) {
		this.vivienda = vivienda;
	}

}
