package ar.edu.unrn.seminario.modelo;

import ModeloException.NullException;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

public class Propietario {

	private String nombre;
	private String apellido;
	private String dni;
	private Usuario usuario;
	private int puntosAcumulados;
	// private PedidoRetiro pedidoRetiro;

	public Propietario(String nombre, String apellido, String dni) throws NullException {
		// agregar expeciones
		if (VerificacionDatos.stringIsEmpty(nombre) || VerificacionDatos.stringIsNull(nombre))
			throw new NullException("nombre");
		if (VerificacionDatos.stringIsEmpty(apellido) ||  VerificacionDatos.stringIsNull(apellido))
			throw new NullException("apellido");
		if (VerificacionDatos.stringIsEmpty(dni) ||  VerificacionDatos.stringIsNull(dni))
			throw new NullException("dni");
		this.apellido = apellido;
		this.dni = dni;
		this.nombre = nombre;

	}
	public Propietario(String nombre, String apellido, String dni,int puntos) throws NullException {
		// agregar expeciones
		if (VerificacionDatos.stringIsEmpty(nombre) || VerificacionDatos.stringIsNull(nombre))
			throw new NullException("nombre");
		if (VerificacionDatos.stringIsEmpty(apellido) ||  VerificacionDatos.stringIsNull(apellido))
			throw new NullException("apellido");
		if (VerificacionDatos.stringIsEmpty(dni) ||  VerificacionDatos.stringIsNull(dni))
			throw new NullException("dni");
		if (VerificacionDatos.IsEmpty(puntos))
			throw new NullException("puntos");
		this.apellido = apellido;
		this.dni = dni;
		this.nombre = nombre;
		this.puntosAcumulados=puntos;

	}

	

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getDni() {
		return dni;
	}
	
	public boolean tienePuntosSuficientes(int puntosBeneficio) {
		if(this.puntosAcumulados >= puntosBeneficio)
			return true;
		else
			return false;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	public void sumarPuntaje(int puntaje) {
		this.puntosAcumulados+=puntaje;
	}
	public void restarPuntaje(int puntaje) {
		this.puntosAcumulados-=puntaje;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public int getPuntosAcumulados() {
		return puntosAcumulados;
	}

	public void setPuntosAcumulados(int puntosAcumulados) {
		this.puntosAcumulados = puntosAcumulados;
	}

	public void setNombre(String nombre) {
		if(nombre!=null || nombre !="")
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		if(apellido!=null)
		this.apellido = apellido;
	}

	public void setDni(String dni) {
		if(dni!=null || dni !="")
		this.dni = dni;
	}

}
