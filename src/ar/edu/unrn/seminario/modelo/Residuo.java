package ar.edu.unrn.seminario.modelo;

import ModeloException.NullException;
import ar.edu.unrn.seminario.dto.TipoResiduoDTO;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

public class Residuo {
	private TipoResiduo tipoResiduo;
	private int id;
	private float peso;
	private Pedido pedido;
	private float pesoRetirado;
	public Residuo(TipoResiduo tipoResiduo, float peso) throws NullException {
		if (VerificacionDatos.IsEmpty(tipoResiduo))
			throw new NullException("es nulo tipo residuo");

		this.tipoResiduo = tipoResiduo;

		this.peso = peso;

	}

	public Residuo(int id, TipoResiduo tipoResiduo, float peso) throws NullException {
		if (VerificacionDatos.IsEmpty(tipoResiduo))
			throw new NullException("es nulo tipo residuo");

		if (VerificacionDatos.IsEmpty(id))
			throw new NullException("el numero es 0");
		this.tipoResiduo = tipoResiduo;
		this.id = id;
		this.peso = peso;

	}
	public Residuo(int id, TipoResiduo tipoResiduo, float peso, float pesoRetirado) throws NullException {
		if (VerificacionDatos.IsEmpty(tipoResiduo))
			throw new NullException("es nulo tipo residuo");

		if (VerificacionDatos.IsEmpty(id))
			throw new NullException("el numero es 0");
		this.tipoResiduo = tipoResiduo;
		this.id = id;
		this.peso = peso;
		this.pesoRetirado = pesoRetirado;

	}

	public int getId() {
		return id;
	}
	public Pedido getPedido() {
		return this.pedido;
	}
	public void setPedido(Pedido pedido) {
		this.pedido=pedido;
	}
	public void setId(int id) {
		this.id = id;
	}

	public TipoResiduo getTipoResiduo() {
		return tipoResiduo;
	}

	public void setTipoResiduo(TipoResiduo tipoResiduo) {
		this.tipoResiduo = tipoResiduo;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}


	
	public float getPesoRetirado() {
		return pesoRetirado;
	}

	public void setPesoRetirado(float pesoRetirado) {
		this.pesoRetirado = pesoRetirado;
	}
	

}