package ar.edu.unrn.seminario.modelo;

import java.time.LocalDateTime;

import ModeloException.NullException;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

public class Vivienda {

	private Propietario propietario;
	private LocalDateTime fechaRegistro;
	private Direccion direccion;
	private int numeroVivienda;

	public Vivienda(int numeroVivienda, Propietario propietario, Direccion direccion) throws NullException {
		if(VerificacionDatos.IsEmpty(numeroVivienda))
			throw new NullException("el numero de vivienda es vacio");
		if(VerificacionDatos.IsEmpty(direccion))
			throw new NullException("la direccion es vacio");
		if(VerificacionDatos.IsEmpty(propietario))
			throw new NullException("el propietario es vacio");
		this.propietario = propietario;
		
		this.direccion = direccion;
		
		this.numeroVivienda = numeroVivienda;
	}

	public Vivienda(Propietario propietario, Direccion direccion) throws NullException {
		if(VerificacionDatos.IsEmpty(direccion))
			throw new NullException("la direccion es vacio");
		if(VerificacionDatos.IsEmpty(propietario))
			throw new NullException("el propietario es vacio");
		this.propietario = propietario;

		this.direccion = direccion;

	}

	public Vivienda(int numeroVivienda) {
		this.numeroVivienda = numeroVivienda;
	}

	public Propietario getPropietario() {
		return propietario;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public int getNumeroVivienda() {
		return this.numeroVivienda;
	}
	
	


	// to string? equals?
}
