package ar.edu.unrn.seminario.modelo;

import java.util.Objects;

import ModeloException.NullException;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

public class Rol {

    private Integer codigo;
    private  String nombre;
    private boolean estado;
    private static final String ADMINISTRADOR = "Administrador";
    private static final String USUARIO = "Comun";
    private static final String RECOLECTOR = "Recolector";
    public Rol() {

	}

    public Rol(Integer codigo, String nombre) throws NullException{
    	if(VerificacionDatos.IsEmpty(codigo))
           throw new NullException("el codigo se encuentra nulo");
        if(VerificacionDatos.IsEmpty(nombre)) {
           throw new NullException("el nombre se encuentra nulo");
        }
        this.codigo=codigo;
        this.nombre=nombre;
    }

    public Rol(int codigo,String nombre, boolean estado) throws NullException{
    	
        if(VerificacionDatos.IsEmpty(codigo))
        throw new NullException("el codigo se encuentra nulo");
        if(VerificacionDatos.IsEmpty(nombre))
        	 throw new NullException("el nombre se encuentra nulo");
        
        	this.estado= estado;
        
        this.nombre=nombre;
        
        this.codigo=codigo;
    	
    }
    public Rol(String nombre, boolean estado) throws NullException{
    	if(VerificacionDatos.IsEmpty(nombre))
       	 throw new NullException("el nombre se encuentra nulo");

        this.nombre=nombre;
        this.estado= estado;
        
    }
    public Integer getCodigo() {
        return codigo;
    }
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}
    
    public void activar() {
        //agregar excepcion
        this.estado=true;
    }
    public void setActivo(boolean activo) {
		this.estado = activo;
	}
    public void desactivar() {
        //agregar excepcion
        this.estado=false;
    }
    public boolean estaActivo() {
        return this.estado;
    }
    public static String administrador() {
    	return ADMINISTRADOR;
    }
    public static String usuario() {
    	return USUARIO;
    }

    public static String recolector() {
    	return RECOLECTOR;
    }
    @Override
    public String toString() {
        return "Rol{" +
                "codigo=" + codigo +
                ", nombre='" + nombre + '\'' +
                ", estado=" + estado +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rol rol = (Rol) o;
        return estado == rol.estado && Objects.equals(codigo, rol.codigo) && Objects.equals(nombre, rol.nombre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, nombre, estado);
    }
}
