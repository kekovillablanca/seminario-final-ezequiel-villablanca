package ar.edu.unrn.seminario.modelo;

import java.util.Date;
import java.util.List;

import ModeloException.EstadoException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

public class OrdenDePedido {

	private Date fechaPedido;
	private int id;
	private Recolector recolectorBasura;
	private List<Visita> visitaVivienda;
	private static final String PENDIENTE = "PENDIENTE";
	private static final String EJECUCION = "EJECUCION";
	private static final String CONCRETADO = "CONCRETADO";
	private static final String CANCELADO = "CANCELADO";
	private Pedido pedido;
	private String estado;

	public OrdenDePedido(Date fechaPedido, Recolector recolector, Pedido pedido) throws NullException {
		if (VerificacionDatos.IsEmpty(recolector)) {
			throw new NullException("recolector");
		}
		if (VerificacionDatos.IsEmpty(pedido)) {
			throw new NullException("pedido");
		}
		this.fechaPedido = fechaPedido;
		this.recolectorBasura = recolector;
		this.pedido = pedido;

	}
	public OrdenDePedido(Date fechaPedido, String estado, int id,Pedido pedido)
			throws NullException {
		if (VerificacionDatos.IsEmpty(fechaPedido)) {
			throw new NullException("pedido");
		}
		if (VerificacionDatos.IsEmpty(estado)) {
			throw new NullException("estado");
		}
		if (VerificacionDatos.IsEmpty(id)) {
			throw new NullException("id");
		}
		
		if (VerificacionDatos.IsEmpty(pedido)) {
			throw new NullException("pedido");
		}
		this.fechaPedido = fechaPedido;
		this.id = id;

		this.estado = estado;
		this.pedido = pedido;
	}

	public OrdenDePedido(Date fechaPedido, String estado, int id) throws NullException {

		if (VerificacionDatos.IsEmpty(id)) {
			throw new NullException("id");
		}
		this.fechaPedido = fechaPedido;
		this.id = id;

		this.estado = estado;

	}

	public OrdenDePedido(Date fechaPedido, Pedido pedido) throws NullException {
		if (VerificacionDatos.IsDateNull(fechaPedido)) {
			throw new NullException("fecha");
		}
		if (VerificacionDatos.IsEmpty(pedido)) {
			throw new NullException("pedido");
		}

		this.fechaPedido = fechaPedido;
		this.pedido = pedido;

	}

	public OrdenDePedido(Date fechaPedido, String estado, int id, Recolector recolector) throws NullException {
		if (VerificacionDatos.IsEmpty(fechaPedido)) {
			throw new NullException("pedido");
		}
		if (VerificacionDatos.IsEmpty(estado)) {
			throw new NullException("estado");
		}
		if (VerificacionDatos.IsEmpty(id)) {
			throw new NullException("id");
		}
		if (VerificacionDatos.IsEmpty(recolector)) {
			throw new NullException("recolector");
		}

		this.fechaPedido = fechaPedido;
		this.id = id;
		this.recolectorBasura = recolector;
		this.estado = estado;
	}

	public OrdenDePedido(Date fechaPedido, String estado, int id, Recolector recolector, Pedido pedido)
			throws NullException {
		if (VerificacionDatos.IsEmpty(fechaPedido)) {
			throw new NullException("pedido");
		}
		if (VerificacionDatos.IsEmpty(estado)) {
			throw new NullException("estado");
		}
		if (VerificacionDatos.IsEmpty(id)) {
			throw new NullException("id");
		}
		if (VerificacionDatos.IsEmpty(recolector)) {
			throw new NullException("recolector");
		}
		if (VerificacionDatos.IsEmpty(pedido)) {
			throw new NullException("pedido");
		}
		this.fechaPedido = fechaPedido;
		this.id = id;
		this.recolectorBasura = recolector;
		this.estado = estado;
		this.pedido = pedido;
	}
	

	public Date getFechaPedido() {
		return this.fechaPedido;
	}

	public void setFechaPedido(Date fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

	public Recolector getRecolectorBasura() {
		return recolectorBasura;
	}

	public void setRecolectorBasura(Recolector recolectorBasura) {
		this.recolectorBasura = recolectorBasura;
	}

	public List<Visita> getVisitaVivienda() {
		return visitaVivienda;
	}

	public void setVisitaVivienda(List<Visita> visitaVivienda) {
		this.visitaVivienda = visitaVivienda;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public static String getPendiente() {
		return PENDIENTE;
	}

	public static String getEjecucion() {
		return EJECUCION;
	}

	public static String getConcretado() {
		return CONCRETADO;
	}

	public static String getCancelado() {
		return CANCELADO;
	}

	public String getEstado() {
		return this.estado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void cambiarEstado(String estado) throws EstadoException {
		if (this.estado == null) {
			this.estado = estado;
		} else if (this.estado.compareTo(this.PENDIENTE) == 0) {
			this.estado = estado;
			if (estado.compareTo(this.CANCELADO) == 0) {
				this.estado = estado;
			}

		} else if (this.estado.compareTo(this.EJECUCION) == 0) {
			this.estado = estado;
			if (estado.compareTo(this.CANCELADO) == 0) {
				this.estado = estado;
			}

		} else if (this.estado.compareTo(this.CONCRETADO) == 0 || estado.compareTo(this.CANCELADO) == 0) {

			throw new EstadoException("no se puede cambiar de estado a cancelado  o se puede registrar visita");

		}
	}
}
