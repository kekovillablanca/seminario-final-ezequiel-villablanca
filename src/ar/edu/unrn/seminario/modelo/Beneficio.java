package ar.edu.unrn.seminario.modelo;

import ModeloException.NullException;
import ar.edu.unrn.seminario.herramienta.VerificacionDatos;

public class Beneficio {
	private String descripcion;
	private Integer puntaje;
	private Integer idBeneficio;


	public Beneficio(String descripcion, Integer puntaje) throws NullException {

		if (VerificacionDatos.stringIsEmpty(descripcion))
			throw new NullException("Descripcion vacia");
		if (VerificacionDatos.IsEmpty(puntaje))
			throw new NullException("Puntaje vacio");
		this.descripcion = descripcion;
		this.puntaje = puntaje;
	}
	
	public Beneficio(String descripcion, Integer puntaje, int idBeneficio) throws NullException {

		if (VerificacionDatos.stringIsEmpty(descripcion))
			throw new NullException("Descripcion vacia");
		if (VerificacionDatos.IsEmpty(puntaje))
			throw new NullException("Puntaje vacio");
		this.descripcion = descripcion;
		this.puntaje = puntaje;
		this.idBeneficio = idBeneficio;
	}
	

	
	
	public String obtenerDescripcion() {
		return this.descripcion;
	}

	public Integer obtenerPuntaje() {
		return this.puntaje;
	}
	public Integer obtenerIdBeneficio() {
		return this.idBeneficio;
	}
}
