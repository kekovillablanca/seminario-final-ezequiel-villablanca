package ar.edu.unrn.seminario.accesos;

import java.util.Date;
import java.util.List;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.modelo.Propietario;
import ar.edu.unrn.seminario.modelo.Usuario;

public interface PropietarioDao {

	void create(Propietario propietario) throws JdbcException, NullException;
	public void create(Usuario usuario,Propietario propietario,UsuarioDao usuarioDao)throws JdbcException;

	boolean update(Propietario propietario) throws JdbcException;



	Propietario find(String dni) throws JdbcException, NullException;

	List<Propietario> findAll() throws NullException, JdbcException;
	List<Propietario> findAll(Usuario usuario) throws NullException, JdbcException;



	int puntosAcumuladosPropietario(String id) throws JdbcException;// Consulta y devuelve los datos acumulados del
																	// propietario id

	

}
