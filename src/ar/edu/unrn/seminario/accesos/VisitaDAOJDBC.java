package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ModeloException.JdbcException;
import ar.edu.unrn.seminario.exception.ConexionException;
import ar.edu.unrn.seminario.modelo.OrdenDePedido;
import ar.edu.unrn.seminario.modelo.Residuo;
import ar.edu.unrn.seminario.modelo.Visita;

public class VisitaDAOJDBC implements VisitaDao {

	@Override
	public void create(Visita visita, OrdenDePedido orden) throws JdbcException {
       
        try {
        	 Connection conn = ConnectionManager.getConnection();
            PreparedStatement statement = conn.prepareStatement(
                    "INSERT INTO visita (dia_concurrido, observacion,legajo_recolector,id_orden)" + " values(?,?,?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);

            statement.setObject(1, visita.getDiaConcurrido());
            statement.setString(2, visita.getObservacion());
            statement.setString(3, orden.getRecolectorBasura().obtenerLegajo());
            statement.setInt(4, orden.getId());
            int cantidad = statement.executeUpdate();
            ResultSet rsVisita = statement.getGeneratedKeys();
            rsVisita.next();
            int idVisita = rsVisita.getInt(1);
            if (cantidad > 0) {
                // System.out.println("Modificando " + cantidad + " registros");
            } else {
            	throw new JdbcException("error de consulta");
              
            }
           
        } catch (SQLException | ConexionException e) {
           throw new JdbcException("error en la consulta"+e.getMessage());
        }
    }



}
