package ar.edu.unrn.seminario.accesos;

import java.sql.SQLException;
import java.util.List;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.modelo.Beneficio;
import ar.edu.unrn.seminario.modelo.Campania;
import ar.edu.unrn.seminario.modelo.Canjeo;


public interface CanjeoDao {

	public void canjearBeneficio(Canjeo canjeo) throws JdbcException;

}
