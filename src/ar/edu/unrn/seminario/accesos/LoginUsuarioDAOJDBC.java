package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.exception.ConexionException;

public class LoginUsuarioDAOJDBC implements LoginUsuarioDao {


	@Override
	public boolean iniciarSesion(String username, String password) throws NullException, JdbcException {
		int idCuenta=0;
		boolean seEncuentra = false;
	
		try {

			Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn
					.prepareStatement("SELECT * FROM cuenta " + " WHERE (usuario=? AND contrasena =?)",PreparedStatement.RETURN_GENERATED_KEYS);
			statement.setString(1, username);
			statement.setString(2, password);
			ResultSet rs = statement.executeQuery();
			
			
			if (rs.next()) {
				if (rs.getString("usuario").equals(username) && (rs.getString("contrasena").equals(password))) {
					seEncuentra=true;
					idCuenta=rs.getInt("id_cuenta");
					
				}

			}
			/*if(seEncuentra==true)
			cambiarEstadoSesion(seEncuentra,idCuenta);*/
			
		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta"+e.getMessage());
		}

		finally {
			ConnectionManager.disconnect();
		}
		
		return seEncuentra;
	}



}



