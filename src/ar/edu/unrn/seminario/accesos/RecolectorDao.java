package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.modelo.Recolector;

public interface RecolectorDao {

	void create(Recolector recolector) throws JdbcException;

	void remove(Long id);

	void remove(Recolector recolector);

	List<Recolector> findAll() throws NullException, JdbcException;

	Recolector find(String legajo) throws JdbcException, NullException;
}
