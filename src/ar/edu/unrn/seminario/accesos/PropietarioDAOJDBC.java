package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.exception.ConexionException;
import ar.edu.unrn.seminario.modelo.Direccion;
import ar.edu.unrn.seminario.modelo.Pedido;
import ar.edu.unrn.seminario.modelo.Propietario;
import ar.edu.unrn.seminario.modelo.TipoResiduo;
import ar.edu.unrn.seminario.modelo.Usuario;
import ar.edu.unrn.seminario.modelo.Vivienda;

public class PropietarioDAOJDBC implements PropietarioDao {
	public void create(Propietario propietario)throws JdbcException {
		try {

			Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn
					.prepareStatement("INSERT INTO propietario(nombre, apellido, dni) " + "VALUES (?, ?, ?)");

			statement.setString(1, propietario.getNombre());
			statement.setString(2, propietario.getApellido());
			statement.setString(3, propietario.getDni());

			int cantidad = statement.executeUpdate();
			if (cantidad > 0) {
				// System.out.println("Modificando " + cantidad + " registros");
			} else {
				System.out.println("Error al actualizar");
				// TODO: disparar Exception propia
			}

		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error en la consulta"+e.getMessage());
			// TODO: disparar Exception propia
		}  finally {
			ConnectionManager.disconnect();
		}

	}
	public void create(Usuario usuario,Propietario propietario,UsuarioDao usuarioDao)throws JdbcException {
		try {
			int idUsuario=usuarioDao.obtenerIdUsuario(usuario);
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn
					.prepareStatement("INSERT INTO propietario(nombre, apellido, dni,id_cuenta) " + "VALUES (?, ?, ?, ?)");

			statement.setString(1, propietario.getNombre());
			statement.setString(2, propietario.getApellido());
			statement.setString(3, propietario.getDni());
			statement.setInt(4, idUsuario);

			int cantidad = statement.executeUpdate();
			if (cantidad > 0) {
				 System.out.println("Modificando " + cantidad + " registros");
			} else {
				System.out.println("Error al actualizar");
				// TODO: disparar Exception propia
			}

		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error en la consulta"+e.getMessage());
			// TODO: disparar Exception propia
		}  finally {
			ConnectionManager.disconnect();
		}

	}


    @Override
    public boolean update(Propietario propietario) throws JdbcException {
    	boolean updateo=false;
    	//modificar update

        try {
            Connection conn = ConnectionManager.getConnection();
            PreparedStatement consulta=conn.prepareStatement(
            		"UPDATE propietario SET nombre = ?,"
            		+ "apellido = ?, id_cuenta=?,puntaje=?  WHERE dni = ?");
            consulta.setString(1,propietario.getNombre());
            consulta.setString(2,propietario.getApellido());
            consulta.setString(3, propietario.getDni());
            if(propietario.getUsuario().getId()==0) 
            	consulta.setNull(4, Types.NULL);
            else
            	consulta.setInt(4, propietario.getUsuario().getId());
            if(propietario.getPuntosAcumulados()==0)
            	consulta.setNull(5, Types.NULL);
            else
            	consulta.setInt(5, propietario.getPuntosAcumulados());
           
            
           if(consulta.executeUpdate()>0) {
        	   updateo=true;
            }
            consulta.close();
            conn.close();
            
        } catch (SQLException | ConexionException e) {
           

            throw new JdbcException("error de consulta"+e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return updateo;
        


    }



	@Override
	public Propietario find(String dni) throws JdbcException, NullException {
		Propietario propietario = null;
		try {
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn.prepareStatement(
					"SELECT * FROM `propietario`"
							+ "WHERE propietario.dni = ?");

			statement.setString(1, dni);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				propietario = new Propietario(rs.getString("propietario.nombre"), rs.getString("propietario.apellido"),
						rs.getString("propietario.dni"),rs.getInt("propietario.puntaje"));
			}

		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta"+e.getMessage());
		} finally {
			ConnectionManager.disconnect();
		}

		return propietario;
	}

	@Override
	public List<Propietario> findAll() throws NullException,JdbcException{

		List<Propietario> propietarios = new ArrayList<Propietario>();
		Statement sentencia = null;
		ResultSet resultado = null;
		try {

			sentencia = ConnectionManager.getConnection().createStatement();
			resultado = sentencia.executeQuery(
					"SELECT * FROM `propietario`" );

			while (resultado.next()) {
				Propietario propietario = new Propietario(resultado.getString("propietario.nombre"),
						resultado.getString("propietario.apellido"), resultado.getString("propietario.dni"));

				propietarios.add(propietario);
			}
		} catch (SQLException | ConexionException  e) {
			throw new JdbcException("error de consulta"+e.getMessage());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}  finally {
			ConnectionManager.disconnect();
		}

		return propietarios;
			

	}
	@Override
	public List<Propietario> findAll(Usuario usuario) throws NullException,JdbcException{
		Propietario propietario = null;
		UsuarioDao usuarioDao = new UsuarioDAOJDBC();
		
		List<Propietario> propietarios = new ArrayList<Propietario>();
		Statement sentencia = null;
		ResultSet resultado = null;
		try {

			Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn.prepareStatement(
					"SELECT * FROM `propietario`"
							+ "WHERE propietario.id_cuenta = ?");

			statement.setInt(1, usuarioDao.obtenerIdUsuario(usuario));
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				propietario = new Propietario(rs.getString("propietario.nombre"), rs.getString("propietario.apellido"),
						rs.getString("propietario.dni"));
				propietarios.add(propietario);
			}
				
			
		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} finally {
			ConnectionManager.disconnect();
		}

		return propietarios;
			

	}


	
	public Propietario findPropietarioPorPedido(int numeroPedido) throws NullException, JdbcException {
			
		Statement sentencia = null;
		ResultSet rs = null;
		Propietario propietario = null;
		try {
			sentencia = ConnectionManager.getConnection().createStatement();

			rs = sentencia.executeQuery(
					"SELECT * FROM propietario join vivienda on pedido_retiro.numero_vivienda=vivienda.numero_vivienda join propietario on propietario.dni=vivienda.dni_propietario\n"
							+ "where pedido_retiro.id_pedido=" + numeroPedido);

			if (rs.next()) {
				propietario=new Propietario(rs.getString("propietario.nombre"),rs.getString("propietario.apellido"), rs.getString("propietario.dni"),rs.getInt("propietario.puntaje"));
		
			}
			rs.close();
			sentencia.close();
		return propietario;
		}catch (

					SQLException | ConexionException e) {
						throw new JdbcException("error de consulta"+e.getMessage());
					}
	
				
			
	
	}

	
	
	@Override
	public int puntosAcumuladosPropietario(String id) throws JdbcException {
		// TODO Auto-generated method stub
		int puntos = 0;

		
		PreparedStatement statement;
		try {
			Connection conn = ConnectionManager.getConnection();
			statement = conn.prepareStatement("SELECT puntaje FROM propietario" + " WHERE dni = ?");
			statement.setString(1, id);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				puntos = rs.getInt("puntaje");
			}
		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta"+e.getMessage());
		}

		return puntos;
	}
	
	

}
