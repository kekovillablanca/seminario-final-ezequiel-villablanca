package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.modelo.Pedido;
import ar.edu.unrn.seminario.modelo.Residuo;
import ar.edu.unrn.seminario.modelo.Vivienda;

public interface PedidoDao {
	public Pedido obtenerPedido(int numeroOrden) throws NullException, JdbcException;
	public void create(Vivienda vivienda, Pedido pedido) throws JdbcException;
	public boolean seEncuentraOrden(int numeroPedido) throws JdbcException;
	List<Pedido> findAll() throws NullException, JdbcException;

	Pedido find(int id_pedido) throws JdbcException, NullException;
	public void update(Pedido pedido) throws JdbcException;
	public List<Residuo> obtenerResiduos(int numeroPedido) throws JdbcException, NullException;
	

}
