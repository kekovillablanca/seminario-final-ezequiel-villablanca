package ar.edu.unrn.seminario.accesos;

import ModeloException.JdbcException;
import ModeloException.NullException;

public interface LoginUsuarioDao {

	boolean iniciarSesion(String username, String password) throws NullException, JdbcException;

	
}
