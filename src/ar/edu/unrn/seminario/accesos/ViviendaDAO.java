package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.modelo.Direccion;
import ar.edu.unrn.seminario.modelo.Propietario;
import ar.edu.unrn.seminario.modelo.Usuario;
import ar.edu.unrn.seminario.modelo.Vivienda;

public interface ViviendaDAO {

	void create(Propietario propietario, Direccion direccion);

	List<Vivienda> findAll();
	public List<Vivienda> findAll(Usuario usuario) throws NullException,JdbcException;
	public Vivienda find(String username);
	public Vivienda findViviendaUsuario() throws JdbcException, NullException;
	public void create(Usuario usuario,Vivienda vivienda, PropietarioDao propietarioDao, DireccionDao direccionDao,UsuarioDao usuarioDao)throws JdbcException, NullException;
}