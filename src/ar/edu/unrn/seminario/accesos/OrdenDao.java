package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.modelo.OrdenDePedido;
import ar.edu.unrn.seminario.modelo.Recolector;

public interface OrdenDao {
	public OrdenDePedido create(OrdenDePedido ordenPedido) throws JdbcException,NullException;

	

	void update(OrdenDePedido ordenDePedido) throws JdbcException;

	public List<OrdenDePedido> findAll(int idUsuario) throws JdbcException, NullException;

	public OrdenDePedido find(int numeroOrden) throws JdbcException, NullException;

	



	boolean estaInCompleto(int orden) throws JdbcException;

	OrdenDePedido findPorAdministrador(int numeroPedido) throws JdbcException, NullException;

}