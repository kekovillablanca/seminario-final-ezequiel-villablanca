package ar.edu.unrn.seminario.accesos;

import ModeloException.JdbcException;
import ar.edu.unrn.seminario.modelo.OrdenDePedido;
import ar.edu.unrn.seminario.modelo.Visita;

public interface VisitaDao {
	public void create(Visita visita, OrdenDePedido orden) throws JdbcException;
}
