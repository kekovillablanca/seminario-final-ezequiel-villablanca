package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ModeloException.ApiException;
import ModeloException.DataEmptyException;
import ModeloException.EmailException;
import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.modelo.Usuario;

public interface UsuarioDao {
	void create(Usuario Usuario) throws JdbcException;

	void update(Usuario Usuario) throws JdbcException;

	void remove(Long id);

	void remove(Usuario Usuario);

	Usuario find(String username) throws JdbcException, NullException, DataEmptyException, EmailException;

	List<Usuario> findAll() throws JdbcException, NullException, DataEmptyException, EmailException;
	public int obtenerIdUsuario(Usuario usuario) throws JdbcException;
	

}
