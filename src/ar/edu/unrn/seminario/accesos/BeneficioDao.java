package ar.edu.unrn.seminario.accesos;

import java.sql.SQLException;
import java.util.List;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.modelo.Beneficio;

public interface BeneficioDao {
	
	void create(Beneficio beneficio) throws JdbcException;
	
	void update (Beneficio beneficio);
	
	Beneficio find(int id)throws JdbcException, NullException;
	
	List<Beneficio> findAll()throws  NullException, JdbcException;

}
