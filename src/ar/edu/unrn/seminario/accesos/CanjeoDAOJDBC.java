package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.exception.ConexionException;
import ar.edu.unrn.seminario.modelo.Beneficio;
import ar.edu.unrn.seminario.modelo.Campania;
import ar.edu.unrn.seminario.modelo.Canjeo;

public class CanjeoDAOJDBC implements CanjeoDao{

	
	@Override
	public void canjearBeneficio(Canjeo canjeo) throws JdbcException {
		// TODO Auto-generated method stub
	
		
		Connection conn=null;
		
			try {
			
			PreparedStatement statement = 
					conn.prepareStatement("INSERT INTO beneficio_canjeado (dni_propietario, id_beneficio, fecha)" 
			+ "VALUES (?, ?, ?)");
			statement.setString(1, canjeo.getPropietario().getDni());
			statement.setInt(2, canjeo.getBeneficio().obtenerIdBeneficio());
			statement.setObject(3,canjeo.getFechaCanejo());
			int cantidadA = statement.executeUpdate();
			if (cantidadA > 0) {
				// System.out.println("Modificando " + cantidad + " registros");
			} else {
				throw new JdbcException("error de consulta");
				// TODO: disparar Exception propia
			}

		} catch (SQLException e) {
			throw new JdbcException("error de consulta"+e.getMessage());
			// TODO: disparar Exception propia
		} finally {
			ConnectionManager.disconnect();
		}
	}

}
