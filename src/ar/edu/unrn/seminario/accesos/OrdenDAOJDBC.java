package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.exception.ConexionException;
import ar.edu.unrn.seminario.modelo.OrdenDePedido;
import ar.edu.unrn.seminario.modelo.Pedido;
import ar.edu.unrn.seminario.modelo.Recolector;
import ar.edu.unrn.seminario.modelo.Usuario;
import ar.edu.unrn.seminario.modelo.Vivienda;

public class OrdenDAOJDBC implements OrdenDao {
	//agregar en pedidoDao y llamar desde el api
	
	

	public List<OrdenDePedido> findAll(int idUsuario) throws JdbcException, NullException {
		List<OrdenDePedido> ordenesDePedido = new ArrayList<>();
		Statement sentencia = null;
		ResultSet resultado = null;
		try {
			sentencia = ConnectionManager.getConnection().createStatement();
			resultado = sentencia.executeQuery(
					"select * from orden_retiro join pedido_retiro on pedido_retiro.id_orden=orden_retiro.id join recolector on recolector.legajo = orden_retiro.legajo_recolector join cuenta c on(c.id_cuenta=recolector.id_cuenta)"
							+ "where recolector.id_cuenta=" + idUsuario);
			while (resultado.next()) {

				ordenesDePedido.add(new OrdenDePedido(resultado.getDate("fecha_pedido"),
						resultado.getString("estado_orden"), resultado.getInt("id"),
						new Recolector(resultado.getString("nombre"), resultado.getString("apellido"),
								resultado.getString("dni"), resultado.getString("recolector.legajo")),
						new Pedido(resultado.getInt("pedido_retiro.id_pedido"))));
			}
		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta" + e.getMessage());
		} finally {

			ConnectionManager.disconnect();
		}

		return ordenesDePedido;

	}

	public OrdenDePedido create(OrdenDePedido ordenPedido) throws JdbcException, NullException {

		try {
			Connection conn = ConnectionManager.getConnection();

			PreparedStatement statement = conn.prepareStatement(
					"INSERT INTO orden_retiro(fecha_pedido, estado_orden) VALUES (?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			statement.setObject(1, ordenPedido.getFechaPedido());
			statement.setString(2, ordenPedido.getEstado());
			int cantidad = statement.executeUpdate();
			ResultSet rsOrden = statement.getGeneratedKeys();
			rsOrden.next();
			int idOrdenPedido = rsOrden.getInt(1);
			rsOrden.close();
			if (cantidad == 1) {
				System.out.println("se a creado correctamente el orden");
			}

			
			
			return new OrdenDePedido(ordenPedido.getFechaPedido(), ordenPedido.getEstado(), idOrdenPedido,ordenPedido.getPedido()); 
		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta" + e.getMessage());
			// TODO: disparar Exception propia
		} finally {

			ConnectionManager.disconnect();
		}
	}

	public void update(OrdenDePedido ordenDePedido) throws JdbcException {

		PreparedStatement statement;
		try {
			Connection conn = ConnectionManager.getConnection();
			statement = conn.prepareStatement("UPDATE `orden_retiro` SET `legajo_recolector`= ?,fecha_pedido=?,estado_orden=? where id=?");
			
			statement.setString(1, ordenDePedido.getRecolectorBasura().obtenerLegajo());
			statement.setObject(2, ordenDePedido.getFechaPedido());
			statement.setString(3, ordenDePedido.getEstado());
			statement.setInt(4, ordenDePedido.getId());

			int cantidad2 = statement.executeUpdate();
			if (cantidad2 == 1) {
				System.out.println("se a creado correctamente");
			}

		} catch (SQLException | ConexionException e1) {
			throw new JdbcException("error de consulta" + e1.getMessage());
		} finally {
			ConnectionManager.disconnect();
		}

	}



	public boolean estaInCompleto(int orden) throws JdbcException {
		int count = 0;
		try {
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn.prepareStatement(
					"SELECT count(ord.id) as cantidad from orden_retiro ord join pedido_retiro pr on ord.id=pr.id_orden join residuo r on pr.id_pedido=r.id_pedido \n"
							+ " WHERE ord.id =?  and r.peso>0");
			statement.setInt(1, orden);

			ResultSet completo = statement.executeQuery();
			if (completo.next()) {
				count = completo.getInt("cantidad");
			}

		} catch (

				SQLException | ConexionException e) {
			throw new JdbcException("error de consulta" + e.getMessage());

		} finally {
			ConnectionManager.disconnect();
		}
		return count > 0;
	}



	public OrdenDePedido find(int numeroOrden) throws JdbcException, NullException {
		OrdenDePedido orden = null;
		try {
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn.prepareStatement(
					"SELECT * from orden_retiro join recolector on orden_retiro.legajo_recolector=recolector.legajo   WHERE orden_retiro.id = ?");

			statement.setInt(1, numeroOrden);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				orden = new OrdenDePedido(rs.getDate("orden_retiro.fecha_pedido"),
						rs.getString("orden_retiro.estado_orden"), rs.getInt("orden_retiro.id"),
						new Recolector(rs.getString("recolector.nombre"), rs.getString("recolector.apellido"),
								rs.getString("recolector.dni"), rs.getString("recolector.legajo")));
			}

		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta" + e.getMessage());
		} finally {
			ConnectionManager.disconnect();
		}
		

		return orden;

	}

	public OrdenDePedido findPorAdministrador(int numeroPedido) throws JdbcException, NullException {
		OrdenDePedido orden = null;
		try {
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn.prepareStatement(
					"SELECT * from  orden_retiro join pedido_retiro on pedido_retiro.id_orden=orden_retiro.id where pedido_retiro.id_pedido=?");

			statement.setInt(1, numeroPedido);

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				orden = new OrdenDePedido(rs.getDate("orden_retiro.fecha_pedido"),
						rs.getString("orden_retiro.estado_orden"), rs.getInt("orden_retiro.id"));
			}

		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta" + e.getMessage());
		} finally {
			ConnectionManager.disconnect();
		}

		return orden;

	}

}