package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import ModeloException.JdbcException;
import ar.edu.unrn.seminario.exception.ConexionException;

public class ConnectionManager {
	private static String DRIVER = "com.mysql.jdbc.Driver";
	private static String URL_DB = "jdbc:mysql://localhost:3307/";
	// jdbc:mysql://localhost:3306/
	protected static String DB = "seminario_viviendas";
	protected static String user = "keko";
	protected static String pass = "12345678";
	protected static Connection conn = null;

	public static void connect() throws ConexionException {
		try {
			conn = DriverManager.getConnection(URL_DB + DB + "?useSSL=false", user, pass);
		} catch (SQLException sqlEx) {
			System.out.println("No se ha podido conectar a " + URL_DB + DB + ". " + sqlEx.getMessage());
			throw new ConexionException("No se ha podido conectar a " + URL_DB + DB + ". " + sqlEx.getMessage());
		}
	}

	public static void disconnect() {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void reconnect() throws ConexionException {
		disconnect();
		connect();
	}

	public static Connection getConnection() throws ConexionException {
		if (conn == null) {
			connect();
		}
		return conn;
	}

}
