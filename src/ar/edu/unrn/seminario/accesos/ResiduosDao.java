package ar.edu.unrn.seminario.accesos;


import java.sql.SQLException;
import java.util.List;

import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.modelo.Residuo;
import ar.edu.unrn.seminario.modelo.TipoResiduo;

public interface ResiduosDao {

	List<TipoResiduo> findAll() throws NullException, JdbcException;

	public List<Residuo> findAllNoLevantados(int numeroOrden) throws  JdbcException, NullException;

	void update(List<Residuo> residuos) throws JdbcException;

}