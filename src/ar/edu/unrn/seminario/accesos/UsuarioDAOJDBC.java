package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ModeloException.ApiException;
import ModeloException.DataEmptyException;
import ModeloException.EmailException;
import ModeloException.JdbcException;
import ModeloException.NullException;
import ar.edu.unrn.seminario.exception.ConexionException;
import ar.edu.unrn.seminario.modelo.Rol;
import ar.edu.unrn.seminario.modelo.Usuario;

public class UsuarioDAOJDBC implements UsuarioDao {

	@Override
	public void create(Usuario usuario) throws JdbcException {
		try {

			Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn.prepareStatement(
					"INSERT INTO cuenta(usuario, contrasena, email, activo,rol) " + "VALUES (?, ?, ?, ?, ?)");

			statement.setString(1, usuario.getNombreUsuario());
			statement.setString(2, usuario.getContrasenia());
			statement.setString(3, usuario.getEmail());
			statement.setBoolean(4, usuario.estaActivo());
			statement.setInt(5, usuario.getRol().getCodigo());
			
			int cantidad = statement.executeUpdate();
			if (cantidad > 0) {
				 
			} else {
				throw new JdbcException("no se creo correctamente  el usuario");
				// TODO: disparar Exception propia
			}

		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta"+e.getMessage());
			// TODO: disparar Exception propia
		} finally {
			ConnectionManager.disconnect();
		}

	}

	@Override
	public void update(Usuario usuario) throws JdbcException {
		

		PreparedStatement statement;
		try {
			Connection conn = ConnectionManager.getConnection();
			statement = conn
					.prepareStatement("UPDATE cuenta SET activo= ?,usuario= ?,contrasenia= ?,email= ?,rol=? where id_cuenta=?");

			statement.setBoolean(1, usuario.estaActivo());
			statement.setString(2, usuario.getNombreUsuario());
			statement.setString(3, usuario.getContrasenia());
			statement.setString(4, usuario.getEmail());
			statement.setInt(5, usuario.getRol().getCodigo());
			statement.setInt(6, usuario.getId());

			int cantidad2 = statement.executeUpdate();
			if (cantidad2 == 1) {
				System.out.println("se a creado correctamente");
			}

		} catch (SQLException | ConexionException e1) {
			throw new JdbcException("error de consulta"+e1.getMessage());
		} finally {
			ConnectionManager.disconnect();
		}
 }



	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void remove(Usuario rol) {
		// TODO Auto-generated method stub

	}

	public int obtenerIdUsuario(Usuario usuario) throws JdbcException{
		
		int id=0;
		try {
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn.prepareStatement(
					"SELECT u.id_cuenta "
							+ " FROM cuenta u" + " WHERE u.usuario = ? AND u.contrasena=? ");
			statement.setString(1, usuario.getNombreUsuario());
			statement.setString(2, usuario.getContrasenia());
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				id=rs.getInt("id_cuenta");
			}
			
			
		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta"+e.getMessage());
		}
		return id;
		
	}
	@Override
	public Usuario find(String username) throws JdbcException, NullException, DataEmptyException, EmailException {
		Usuario usuario = null;
		try {
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement statement = conn.prepareStatement(
					"SELECT u.id_cuenta,u.usuario,  u.contrasena, u.email, u.activo , r.codigo as codigo_rol, r.nombre as nombre_rol "
							+ " FROM cuenta u JOIN roles r ON (u.rol = r.codigo) " + " WHERE u.usuario = ?");

			statement.setString(1, username);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				Rol rol;
				
				
				
			
					rol = new Rol(rs.getInt("codigo_rol"), rs.getString("nombre_rol"));
					usuario = new Usuario(rs.getInt("id_cuenta"),rs.getString("usuario"), rs.getString("contrasena"), rs.getString("email"), rol,rs.getBoolean("activo"));
				
			}

		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta"+e.getMessage());

		
		}  finally {
			ConnectionManager.disconnect();
		}

		return usuario;
	}

	@Override
	public List<Usuario> findAll() throws JdbcException, NullException, DataEmptyException, EmailException {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		try {
			Connection conn = ConnectionManager.getConnection();
			Statement statement = conn.createStatement();
			ResultSet rs = statement.executeQuery(
					"SELECT * FROM `cuenta` as c join roles as r on c.rol=r.codigo");

			while (rs.next()) {

				Rol rol = new Rol(rs.getInt("r.codigo"), rs.getString("r.nombre"),rs.getBoolean("r.activo"));
				Usuario usuario;
		
					usuario = new Usuario(rs.getString("c.usuario"), rs.getString("c.contrasena"),
							rs.getString("c.email"), rol,rs.getBoolean("c.activo"));
					usuarios.add(usuario);
			

				
			}
		} catch (SQLException | ConexionException e) {
			throw new JdbcException("error de consulta"+e.getMessage());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			// TODO: disparar Exception propia
		}  finally {
			ConnectionManager.disconnect();
		}

		return usuarios;
	}


	}

