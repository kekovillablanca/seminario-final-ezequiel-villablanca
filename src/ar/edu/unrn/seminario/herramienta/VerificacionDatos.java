package ar.edu.unrn.seminario.herramienta;

import java.util.ArrayList;
import java.util.Date;

import ar.edu.unrn.seminario.modelo.Beneficio;

public class VerificacionDatos {

	
	public static boolean stringIsEmpty(String s) {
		return s.equals("") || s.equals(" ");
	}
	
	public static boolean IsEmpty (Object obj) {
		return obj.equals("");
	}
	
	public static boolean IsEmpty (int number) {
		return number==0;
	}
	
	public static boolean stringIsNull(String s){
		return  s == null || s.isEmpty();
	}
	public static boolean IsDateNull(Date d) {
		return d.equals(null);
	}
	public static boolean IsActive(boolean state) {
		return state;
	}
	public static boolean IsActive(String state) {
		return state.equals("activo");
	}
	public static boolean IsInactive(boolean state) {
		return !state;
	}
	public static boolean IsInactive(String state) {
		return state.equals("inactivo");
	}
	public static boolean IsNull(Object obj) {
		return obj == null ;
	}
	public static boolean IsIncorrectEmail(String email) {
		return !(email.contains("@") &&  email.charAt(0) !='@' && !email.contains("@." )&&
		(email.endsWith(".com") |email.endsWith(".ar")));
			
	}
	
	public static boolean IsNotNumber(String number) {
        boolean validacion;

        try {
            Integer num = Integer.parseInt(number);
            validacion = false;
        }
        catch(NumberFormatException e) {
            return true;
        }

        return validacion;

    }
}
